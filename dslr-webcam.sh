#!/bin/bash

# Inspired by
# https://svij.org/blog/2020/11/22/canon-dslr-als-webcam-auf-linux-systemen-verwenden/
# and
# https://maximevaillancourt.com/blog/canon-dslr-webcam-debian-ubuntu

# Prerequisites: gphoto2 v4l2loopback-dkms ffmpeg vlc

# Observations
# Seems like shutter speed is fixed at 1/30 (30 fps), changing aperture also has no effect.
# --> need to use ISO setting to account for brightness. Auto setting works decently.

device_number=10
device="/dev/video${device_number}"
cam_displayname="Canon EOS 60D"

if [[ -z "${TRY_STUFF}" ]]; then
  # Load v4l2loopback kernel module and create /dev/video10
  sudo rmmod -s v4l2loopback
  sudo modprobe v4l2loopback devices=1 video_nr=${device_number} card_label="${cam_displayname}" exclusive_caps=1
  if [ ! -e /dev/video10 ]; then
    echo "Error while creating ${device}."
    exit 1
  fi

  # Query some info from the camera
  cameramodel=$(gphoto2 --get-config=/main/status/cameramodel | grep '^Current:' | cut -b 10-)
  if [[ -z "${cameramodel}" ]]; then
    echo "Forgot to switch on the camera?"
    exit 1
  fi
  if [[ ${cameramodel} != ${cam_displayname} ]]; then
    echo "Unexpected camera model: ${cameramodel}"
    exit 1
  fi
  echo "Camera model: ${cameramodel}"
  lensname=$(gphoto2 --get-config=/main/status/lensname | grep '^Current:' | cut -b 10-)
  echo "Lens name   : ${lensname}"
  iso=$(gphoto2 --get-config=/main/imgsettings/iso | grep '^Current:' | cut -b 10-)
  echo "ISO setting : ${iso}"
  if [[ ${iso} != "Auto" ]]; then
    echo "  Setting ISO to Auto..."
    gphoto2 --set-config=/main/imgsettings/iso=0
    iso=$(gphoto2 --get-config=/main/imgsettings/iso | grep '^Current:' | cut -b 10-)
    echo "  New ISO setting : ${iso}"
  fi

  sleep 2
  echo "Looks good - starting to stream from the camera to ${device}..."
fi

# Launch VLC (as preview) in the background, but with 5s delay so that the ffmpeg stream is in place by then
( sleep 5 && cvlc v4l2://${device} --live-caching 10 ) &


# Get the stream from the camera via USB with gphoto2 and pipe it into ffmpeg so that it becomes available at ${device} like a webcam stream
# Use an ffmpeg filter to pad the image so that instead of a weird 1056x704 resolution we get webcam-typical 1280x720 (albeit with some bars left and right).
# Optionally add a chromakeyed overlay (0xffffff = white - change to 0x000000 for black) from a ~/video_bg.png (1280x720) to add some company branding
# (if file is present).
bg_image="${HOME}/video_bg.png"
if [ -f "${bg_image}" ]; then
  echo "Overlaying ${bg_image}."
  gphoto2 --stdout --capture-movie | \
   ffmpeg -i "${bg_image}" -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 \
          -filter_complex "[0:0]chromakey=0xffffff:0.01:0.0[ckout];[1:0]scale=height=720,pad=width=1280:height=720:x=out_w/2-in_w/2:y=out_h/2-in_w/2:color=white[padded];[padded][ckout]overlay[out]" -map [out] \
          ${device}
else
  gphoto2 --stdout --capture-movie | \
   ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 \
          -vf "pad=in_w:in_h+16:0:-8:white,pad=ih*16/9:ih:(ow-iw)/2:(oh-ih)/2:white" \
          ${device}
fi
